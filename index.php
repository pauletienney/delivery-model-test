<?php

require(DeliveryModel.php);

// The result
$price = null;

// The class you have to build
$deliveryModel = new DeliveryModel();

$date = new \DateTime();

$canDeliver = $deliveryModel->canDeliver($date);

if ($canDeliver) {

    $price = $deliveryModel->deliveryPrice($date);

}

echo $price;


?>