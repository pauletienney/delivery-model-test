## Exercice : modèles de livraison

Créer une class PHP simplifiée appelée DeliveryModel ayant pour rôle de gérer les méthodes de livraison sur Prestachef. Cette classe sera appelée et utilisée depuis index.php

Elle aura pour rôle de dire si oui ou non une livraison est possible tel jour. Si la livraison est possible, il faudra déterminer les frais de port.

N'importe quelle date à venir dans les 5 prochaines années doit pouvoir être prise en compte.

Les données : 

# Jours de semaine de livraison :

- Lundi : non
- Mardi : oui
- Mercredi : oui
- Jeudi : oui
- Vendredi : oui
- Samedi : oui
- Dimanche : non

# Montant des frais de port :

- Lundi : -
- Mardi : 50
- Mercredi : 50
- Jeudi : 50
- Vendredi : 50
- Samedi : 100
- Dimanche : -

# Jours de semaine de livraison et frais de ports associés :

**Il faut prendre en compte les jours fériés, avec leur particularités. Le comportement des jours fériés est prioritaire sur celui des jours de semaine (ex : si Noël tombe un dimanche alors la livraison est possible)**

- 1er janvier : oui - 100
- Paques : oui - 70
- Lundi de Pâques : oui - 100
- 1er mai : non
- 8 mai : oui - 70
- Ascencion : oui - 70
- Lundi de pentcôte : non
- 14 juillet : oui - prix habituels, minimum 50 euros
- 15 aout : oui - prix habituels, minimum 50 euros
- 1er novembre : non
- 11 novembre : non
- 25 décembre : oui - 300 euros